//
//  Petitions.swift
//  Project07
//
//  Created by Cesar Palma on 3/5/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import Foundation

struct Petitions: Codable {
    var results: [Petition]
}
