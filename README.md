# My take on 100DaysOfSwift

This is my *go the extra mile* attempt using [Paul Hudson](https://twitter.com/twostraws)'s initiative [100DaysOfSwift](https://www.hackingwithswift.com/100).


## Who I am

I'm one of those guys who loves to code. I have a Pharmacy Degree and I'm currently working in Clinical Research.  

**WWDC2014**... Apple announces Swift... I am amazed and excited.... wait, I'm not a developer, I make a living researching new and exciting drugs for the benefit 
of all human beings... why am I excited?

My wife - *You should make me an app.*

Me - *Sounds like fun... i'll do it*

So I started the "self taught" journey... after 6 months I had an app, it was a timer app in preparation for a GMAT exam. The main idea was to record how much time you spend in each question. At the end, you were able to identify which questions or topics were weak and work on that to improve knowledge and time.
Also the app had a subtle *ding* every 2 minutes in oder to create that awarenes of the time you had per question.  Anyways, it was a hit with my wife.


## Why Am I doing this?
Because it's fun. I'm taking the step to engage with the community even if I'm not a full-time developer. I created this repository to share and discuss my approach to Paul's examples. You can follow me [@R4S3C_Tweets](https://twitter.com/R4S3C_Tweets).

## Inspiration
After these many years following developers from my own shadow I learned that at some point you can not improve if you don't have a mentor or if you are not involved in the community. I want to thank my mentors, even knowing they don't know the impact they've had in my iOS development journey.

1. [Paul Hudson](https://twitter.com/twostraws)
2. [Sean Allen](https://twitter.com/seanallen_dev)
3. [Ray Wenderlich](https://twitter.com/rwenderlich)
4. [Mark Moeykens](https://twitter.com/bigmtnstudio)

## How to help?
Add me on [Twitter](https://twitter.com/R4S3C_Tweets), create pull requests, comment my code, send funny memes.
