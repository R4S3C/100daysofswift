//
//  Flag.swift
//  Challenge Day 23
//
//  Created by Cesar Palma on 2/24/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import Foundation

struct Flag {
    let imageName: String
    let countryName: String
    
    init(withImageName flagImageName: String) {
        self.imageName = flagImageName
        self.countryName = flagImageName.replacingOccurrences(of: ".png", with: "").replacingOccurrences(of: "_", with: " ")
    }
}
