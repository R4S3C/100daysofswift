//
//  Storyboarded.swift
//  Challenge Day 23
//
//  Created by Cesar Palma on 2/24/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit

protocol Storyboarded {
    static func instantiate() -> Self
}

extension Storyboarded {
    static func instantiate() -> Self {
        let id = String(describing: self)
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: id) as! Self
    }
}
