//
//  Helpers.swift
//  Challenge Day 23
//
//  Created by Cesar Palma on 2/24/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import Foundation

extension FileManager {
    
    static func getFlags() -> [Flag] {
        var flags = [Flag]()
        let fm = FileManager.default
        let path = Bundle.main.resourcePath!

        let items = try! fm.contentsOfDirectory(atPath: path)
        for imageName in items {
            if imageName.hasSuffix("png") {
                let flag = Flag(withImageName: imageName)
                flags.append(flag)
            }
        }
        return flags
    }
    
}

