//
//  ViewController.swift
//  Challenge Day 23
//
//  Created by Cesar Palma on 2/24/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    var flags = [Flag]()

    override func viewDidLoad() {
        super.viewDidLoad()
        flags = FileManager.getFlags()
        
        title = "World Flags"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flags.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Country", for: indexPath)
        cell.textLabel?.text = flags[indexPath.row].countryName
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = DetailViewController.instantiate()
        detailVC.selectedFlag = flags[indexPath.row]
        navigationController?.pushViewController(detailVC, animated: true)
    }


}

