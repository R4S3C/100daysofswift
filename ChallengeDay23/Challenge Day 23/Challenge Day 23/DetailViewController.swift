//
//  DetailViewController.swift
//  Challenge Day 23
//
//  Created by Cesar Palma on 2/24/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, Storyboarded {
    
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var countryNameLabel: UILabel!
    
    var selectedFlag: Flag?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let flag = selectedFlag {
            flagImageView.image = UIImage(named: flag.imageName)
            countryNameLabel.text = flag.countryName
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareTapped))
        navigationItem.largeTitleDisplayMode = .never
    }
    
    @objc func shareTapped() {
        guard let flag = selectedFlag else {fatalError("No flag was found")}
        let message = "Look! this is the flag of \(flag.countryName)"
        let image = UIImage(named: flag.imageName)!
        let activityCV = UIActivityViewController(activityItems: [message, image], applicationActivities: [])
        
        activityCV.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        present(activityCV, animated: true, completion: nil)
    }
    
}
