//
//  PersonCell.swift
//  Project10
//
//  Created by Cesar Palma on 3/14/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit

class PersonCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var name: UILabel!
    
}
