//
//  UIView-Extension.swift
//  Project08
//
//  Created by Cesar Palma on 3/9/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit

extension UIView {
    func zoomInAndDissapear() {
        UIView.animate(withDuration: 0.4, animations: {
            self.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }) { (_) in
            self.isHidden = true
        }
    }
    
    func appearAndZoomOut() {
        self.isHidden = false
        UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            self.transform = CGAffineTransform.identity
        })
    }
    
    func bounce() {
        UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform(scaleX: 4.0, y: 4.0)
            self.transform = CGAffineTransform.identity
        })
    }
    
}

