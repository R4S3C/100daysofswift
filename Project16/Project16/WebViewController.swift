//
//  WebViewController.swift
//  Project16
//
//  Created by Cesar Palma on 4/4/19.
//  Copyright © 2019 RɅSΞC. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate, Storyboarded {

    var webView: WKWebView!
    
    var placeName: String?
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate =  self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        
        guard let websiteToLoad = placeName, let url = URL(string: "https://wikipedia.org/wiki/" + websiteToLoad) else { return }
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        #warning("A lot to work on this but at least the challenge was completed.")
        
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        title = webView.title
    }

}
