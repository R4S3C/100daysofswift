//
//  ViewController.swift
//  Project01
//
//  Created by Cesar Palma on 2/16/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    var pictures = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Storm Viewer"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareTheApp))
        
        
        let fileManager = FileManager.default
        let path = Bundle.main.resourcePath!
        
        let items = try! fileManager.contentsOfDirectory(atPath: path)
        
        for item in items {
            if item.hasPrefix("nssl") {
                pictures.append(item)
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pictures.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Picture", for: indexPath)
        cell.textLabel?.text = pictures[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = DetailViewController.instantiate()
        detailVC.selectedImage = pictures[indexPath.row]
        detailVC.pictures =  pictures
        detailVC.selectedImageIndex = indexPath.row + 1
        
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
    @objc private func shareTheApp() {
        let message = "Learn Swift and iOS Develepment with Hacking with Swift."
        let url = URL(string: "https://www.hackingwithswift.com/100")!
        let image = #imageLiteral(resourceName: "Icon")
        let activityVC = UIActivityViewController(activityItems: [message, url, image], applicationActivities: [])
        activityVC.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        present(activityVC, animated: true, completion: nil)
    }
    
}

