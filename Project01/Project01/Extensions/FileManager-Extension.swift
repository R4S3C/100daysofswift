//
//  FileManager-Extension.swift
//  Project01
//
//  Created by Cesar Palma on 2/17/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import Foundation

extension FileManager {
    static func fetchImageNames() -> [String] {
        var pictureNames = [String]()
        
        
        let fileManager = FileManager.default
        let path = Bundle.main.resourcePath!
        
        
        let items = try! fileManager.contentsOfDirectory(atPath: path)
        
        for item in items {
            if item.hasPrefix("nssl") {
                pictureNames.append(item)
            }
        }
    
        return pictureNames
        
    }
}
