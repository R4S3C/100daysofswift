//
//  DetailViewController.swift
//  Project01
//
//  Created by Cesar Palma on 2/17/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, Storyboarded {

    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            guard let imageName = selectedImage else { fatalError("No image was found") }
            imageView.image = UIImage(named: imageName)
            title = setTitle(for: selectedImageIndex, in: pictures)
        }
    }
    
    var selectedImage: String!
    var selectedImageIndex: Int!
    var pictures = [String]()
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        navigationController?.hidesBarsOnTap = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .never
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.hidesBarsOnTap = false
    }
    
    private func setTitle(for selectedImageIndex: Int, in pictures: [String]) -> String {
        return "Picture \(selectedImageIndex) of \(pictures.count)"
    }
    
}
