# Storm Viewer

## My take

* `Storyboarded` Protocol used to instantiate `DetailViewController`.
* `FileManager` extension created to fetchImageNames
* In `DetailViewController` when selectedImage has a value:
    - `UIImageView` is set. 
    - ~~title has not the file extension in the name.~~
    - `title` shows “Picture X of Y”, where Y is the total number of images and X is the selected picture’s position in the array. 
* `ViewController` now has an ordered list of pictures. 
* `UITableViewcell` text changed to `Headline`
* imageView  marked as `clipToBounds` in the Storyboard.
* Added AppIcon
* Added LaunchScreen

## Screenshots

![Icon] (https://bytebucket.org/R4S3C/100daysofswift/raw/4a85275fa80cba6ff4fc6bdb7f30e1337557877c/Project01/screenshots/01.png)
![LaunchScreen] (https://bytebucket.org/R4S3C/100daysofswift/raw/4a85275fa80cba6ff4fc6bdb7f30e1337557877c/Project01/screenshots/02.png)
![ViewControler] (https://bytebucket.org/R4S3C/100daysofswift/raw/a22522a715e2f202d059421b43f7354b9091e1e7/Project01/screenshots/03.png)
![DetailViewController] (https://bytebucket.org/R4S3C/100daysofswift/raw/a22522a715e2f202d059421b43f7354b9091e1e7/Project01/screenshots/04.png)


