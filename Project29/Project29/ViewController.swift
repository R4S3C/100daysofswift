//
//  ViewController.swift
//  Project29
//
//  Created by Cesar Palma on 5/5/19.
//  Copyright © 2019 RɅSΞC. All rights reserved.
//


// https://github.com/jrendel/SwiftKeychainWrapper

import LocalAuthentication
import UIKit

class ViewController: UIViewController {
    
    
    
    @IBOutlet var secret: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addKeyboardObservers(to: .default)
        NotificationCenter.default.addObserver(self, selector: #selector(saveSecretMessage), name: UIApplication.willResignActiveNotification, object: nil)
        
        title = "Nothing to se here"
    }
    
    deinit {
        removeKeyboardObservers(from: .default)
    }

    @IBAction func authenticateTapped(_ sender: UIButton) {
        
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identify Yourself"
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { [weak self] (success, authenticationError) in
                DispatchQueue.main.async {
                    if success {
                        self?.unlockSecretMessage()
                    } else {
                        // Error with boimetric authentication
                        let ac = UIAlertController(title: "Authentication failed", message: "You could not be verified; please try again.", preferredStyle: .alert)
                        ac.addAction(UIAlertAction(title: "OK", style: .default))
                        self?.present(ac, animated: true)
                    }
                }
            }
        }
        else {
            // No biometry detected on device
            let ac = UIAlertController(title: "Biometry unavailable", message: "Your device is not configured for biometric authentication.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        }
    }
    
    fileprivate func unlockSecretMessage() {
        secret.isHidden = false
        title = "Secret Stuff!"
        secret.text = KeychainWrapper.standard.string(forKey: "SecretMessage") ?? ""
    }
    
    @objc fileprivate func saveSecretMessage() {
        guard secret.isHidden == false else { return }
        
        KeychainWrapper.standard.set(secret.text, forKey: "SecretMessage")
        secret.resignFirstResponder()
        secret.isHidden = true
        title = "Nothing to se here"
    }
    
}

extension ViewController: KeyboardObserving {
    func keyboardWillShow(with keyboardSize: CGRect) {
        let keyboardViewEndFrame = view.convert(keyboardSize, from: view.window)
        secret .contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        secret.scrollIndicatorInsets = secret.contentInset
        secret.scrollRangeToVisible(secret.selectedRange)

    }
    
    func keyboardWillHide() {
        secret.contentInset = .zero
        secret.scrollIndicatorInsets = secret.contentInset
        secret.scrollRangeToVisible(secret.selectedRange)
    }
    
}



