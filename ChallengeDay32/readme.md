# Challenge Day 32

## My take
- Added and `Alert` abstraction to manage alerts
- When you tap a cell the item is marked as completed with a checkmark.
- Now you can delete items
- When the leftButton is pressed the app shows an alert before delete all your items
- leftButton is show only if you have items in your shopping list.
- It's not allowed to add an empty item, an alert is shown.
- BarStyle set on `.black`






