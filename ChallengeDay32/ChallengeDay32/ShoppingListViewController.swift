//
//  ViewController.swift
//  ChallengeDay32
//
//  Created by Cesar Palma on 3/5/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit

class ShoppingListViewController: UITableViewController {
    
    var items = [String]()
    var shareItem: UIBarButtonItem!
    let hapticFeedback = UINotificationFeedbackGenerator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Shopping List"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barStyle = .black
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteAllItems))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addItem))
        
        shareItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareTapped))
        
        setupToolBar()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        navigationItem.leftBarButtonItem?.isEnabled = items.isEmpty ? false : true
        shareItem.isEnabled = items.isEmpty ? false : true
    }
    
    private func setupToolBar() {
        navigationController?.isToolbarHidden = false
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbarItems = [spacer, shareItem]
    }
    
    @objc private func shareTapped() {
        let messageToShare = "Look at these Shopping List items:\n- \(items.joined(separator: "\n- "))"
        let activity = UIActivityViewController(activityItems: [messageToShare], applicationActivities: nil)
        activity.popoverPresentationController?.barButtonItem = shareItem
        present(activity, animated: true, completion: nil)
    }
    
    @objc private func addItem() {
        hapticFeedback.notificationOccurred(.success)
        let ac = UIAlertController(title: "Add a new item", message: nil, preferredStyle: .alert)
        ac.addTextField()
        ac.addAction(UIAlertAction(title: "Add", style: .default, handler: { [weak self, weak ac] (_) in
            guard let item = ac?.textFields?.first?.text else { return }
            if item == "" {
                Alert.noTextOntextField(on: self!)
                return
            }
            self?.items.insert(item, at: 0)
            let indexPath = IndexPath(row: 0, section: 0)
            self?.tableView.insertRows(at: [indexPath], with: .automatic)
        }))
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(ac, animated: true)
    }
    
    @objc private func deleteAllItems() {
        Alert.deletingAllItems(on: self) { [weak self] (_) in
            self?.items.removeAll(keepingCapacity: true)
            self?.tableView.reloadData()
        }
    }
    
}

extension ShoppingListViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Item", for: indexPath)
        cell.accessoryType = .none
        cell.textLabel?.text = items[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else { return }
        cell.accessoryType = cell.accessoryType == .none ? .checkmark : .none
        //let hapticFeedback = UINotificationFeedbackGenerator()
        hapticFeedback.notificationOccurred(.success)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            items.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
           // let hapticFeedback = UINotificationFeedbackGenerator()
            hapticFeedback.notificationOccurred(.warning)
        }
        
        
    }
    
}
