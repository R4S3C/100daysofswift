//
//  Alert.swift
//  ChallengeDay32
//
//  Created by Cesar Palma on 3/5/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit

struct Alert {
    private static func showBasicAlert(on vc: UIViewController, with title: String, message: String, actiontitle: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: actiontitle, style: .default, handler: nil))
        DispatchQueue.main.async {
            vc.present(alert, animated: true)
        }
    }
    private static func showBasicDeleteAlert(on vc: UIViewController, with title: String, message: String, actiontitle: String, handler: @escaping (UIAlertAction) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: actiontitle, style: .destructive, handler: handler))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        DispatchQueue.main.async {
            vc.present(alert, animated: true)
        }
    }
    
    static func noTextOntextField(on vc: UIViewController) {
        showBasicAlert(on: vc, with: "Forgetting something?", message: "A text is required", actiontitle: "OK")
    }
    
    static func deletingAllItems(on vc: UIViewController, handler: @escaping (UIAlertAction) -> Void ) {
        showBasicDeleteAlert(on: vc, with: "Are you sure?", message: "All items will be deleted", actiontitle: "Yes, I'm sure", handler: handler)
    }
}

