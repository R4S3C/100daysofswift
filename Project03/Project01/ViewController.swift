//
//  ViewController.swift
//  Project01
//
//  Created by Cesar Palma on 2/16/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    // MARK: - Properties
    
    var pictures = [String]()
    
     // MARK: - App Life Cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pictures = FileManager.fetchImageNames()
        title = "Storm Viewer"
        navigationController?.navigationBar.prefersLargeTitles = true
        
    }
    
    // MARK: - UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pictures.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Picture", for: indexPath)
        cell.textLabel?.text = pictures[indexPath.row]
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = DetailViewController.instantiate()
        detailVC.selectedImage = pictures[indexPath.row]
        detailVC.pictures =  pictures
        detailVC.selectedImageIndex = indexPath.row + 1
        
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
}

