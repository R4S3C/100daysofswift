//
//  DetailViewController.swift
//  Project01
//
//  Created by Cesar Palma on 2/17/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, Storyboarded {

    // MARK: - Outlets
    
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            guard let imageName = selectedImage else { fatalError("No image was found") }
            imageView.image = UIImage(named: imageName)
            title = setTitle(for: selectedImageIndex, in: pictures)
        }
    }
    
    // MARK: - Properties
    
    var selectedImage: String!
    var selectedImageIndex: Int!
    var pictures = [String]()
   
    // MARK: - App Life cicle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        navigationController?.hidesBarsOnTap = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareTapped))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.hidesBarsOnTap = false
    }
    
    // MARK: - Functions
    
    private func setTitle(for selectedImageIndex: Int, in pictures: [String]) -> String {
        return "Picture \(selectedImageIndex) of \(pictures.count)"
    }
    
    // MARK: - Selectors
    
    @objc private func shareTapped() {
        
        guard let image = imageView.image?.jpegData(compressionQuality: 0.8), let imageName = selectedImage else {
            print("No image was found")
            return
        }
        
        let message = "\(imageName) shared by Storm Viewer"
        
        let activityVC = UIActivityViewController(activityItems: [image, message], applicationActivities: [])
        activityVC.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        present(activityVC, animated: true, completion: nil)
    }
    
}
