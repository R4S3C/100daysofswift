//
//  Alert.swift
//  Project04
//
//  Created by Cesar Palma on 2/26/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit
// Tutorial by Sean Allen: https://www.youtube.com/watch?v=ZBS2uRP6_2U
struct Alert {
    private static func showBasicAlert(on vc: UIViewController, with title: String, message: String, actiontitle: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: actiontitle, style: .default, handler: nil))
        DispatchQueue.main.async {
            vc.present(alert, animated: true)
        }
    }
    
    static func showNotAllowedWebsite(on vc: UIViewController) {
        showBasicAlert(on: vc, with: "🚫 Website not allowed", message: "If you care about privacy, stay away of this website", actiontitle: "Keep me safe")
    }
    static func showNavigationNotAllowed(on vc: UIViewController) {
        showBasicAlert(on: vc, with: "No navigation is allowed", message: "The content of this website has been blocked", actiontitle: "OK")
    }
    
}
