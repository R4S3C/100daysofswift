//
//  HomeViewController.swift
//  Project04
//
//  Created by Cesar Palma on 2/26/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit

class HomeViewController: UITableViewController {
    
    // MARK: - Properties
    
    var websites = ["apple.com", "hackingwithswift.com", "duckduckgo.com", "google.com", "facebook.com"]
    var websitesAllowed = ["apple.com", "hackingwithswift.com", "duckduckgo.com"]
    
    // MARK: - App Life Cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Easy Browser"
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return websites.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Website", for: indexPath)
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = websites[indexPath.row]
        return cell
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath), let website = cell.textLabel?.text else { fatalError("No text in cell was found") }
        if websitesAllowed.contains(website) {
            let webViewVC = WebViewController.instantiate()
            webViewVC.websiteSelected = website
            webViewVC.websitesAllowed  = websitesAllowed
            navigationController?.pushViewController(webViewVC, animated: true)
        } else {
            Alert.showNotAllowedWebsite(on: self)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
