//
//  WebViewController.swift
//  Project04
//
//  Created by Cesar Palma on 2/25/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, Storyboarded {
    
    // MARK: - Properties
    
    var webView: WKWebView!
    var progressView: UIProgressView!
    var websitesAllowed = [String]()
    var websiteSelected: String?
    
    //MARK: - App life cicle
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let websiteToLoad = websiteSelected, let url = URL(string: "https://" + websiteToLoad) else { return }
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "open", style: .plain, target: self, action: #selector(openTapped))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpToolbar()
    }
    
    // MARK: - Functions
    
    private func openPage(action: UIAlertAction) {
        let url = URL(string: "https://" + action.title!)!
        webView.load(URLRequest(url: url))
    }
    
    private func setUpToolbar() {
        let goBack = UIBarButtonItem(image: #imageLiteral(resourceName: "left-arrow"), style: .plain, target: self, action: #selector(handleGoBack))
         let goForward = UIBarButtonItem(image: #imageLiteral(resourceName: "right-arrow"), style: .plain, target: self, action: #selector(handleGoForward))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))

        progressView = UIProgressView(progressViewStyle: .default)
        progressView.sizeToFit()
        let progressButtonItem = UIBarButtonItem(customView: progressView)
        
        toolbarItems = [goBack, spacer, goForward, spacer, progressButtonItem, spacer, refresh]
        navigationController?.isToolbarHidden = false
        
        toolbarItems?[0].isEnabled = webView.canGoBack ? true : false
        toolbarItems?[2].isEnabled = webView.canGoForward ? true : false
    }
    
    // MARK: - Selectors
    
    @objc private func handleGoBack() {
        if webView.canGoBack {
            webView.goBack()
        }
        
    }
    
    @objc private func handleGoForward() {
        if webView.canGoForward {
            webView.goForward()
        }
    }
    
    @objc private func openTapped() {
        let ac = UIAlertController(title: "Open page...", message: nil, preferredStyle: .actionSheet)
        for website in websitesAllowed {
            ac.addAction(UIAlertAction(title: website, style: .default, handler: openPage))
        }
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        ac.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        present(ac, animated: true)
    }
    
    // MARK: - Observers
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            progressView.progress = Float(webView.estimatedProgress)
        }
    }
    
}

extension WebViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        title = webView.title
        setUpToolbar()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        let url = navigationAction.request.url
        
        if let host = url?.host {
            for website in websitesAllowed {
                if host.contains(website) {
                    decisionHandler(.allow)
                    return
                }
            }
        }
        decisionHandler(.cancel)
        Alert.showNavigationNotAllowed(on: self)
    }
}


