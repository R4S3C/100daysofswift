# Guess the Flag
Project 02
## My take
- Custom button subclass to manage button behaviour and appearance. 
- Added `setFlag(for:)` to simplify setting an image for custom button.
- Play again button have a custom appearance.
- String extension to manage capitalization of US and UK

## Screenshots
![](https://bytebucket.org/R4S3C/100daysofswift/raw/fd44341a26e004bd3db29addff71e04cac141016/Project02/screenshots/00.png "")
![](https://bytebucket.org/R4S3C/100daysofswift/raw/4a7e8cd0379d3e64690eaf248409df33e4243cdc/Project02/screenshots/01.png "")
![](https://bytebucket.org/R4S3C/100daysofswift/raw/4a7e8cd0379d3e64690eaf248409df33e4243cdc/Project02/screenshots/02.png "")
![](https://bytebucket.org/R4S3C/100daysofswift/raw/4a7e8cd0379d3e64690eaf248409df33e4243cdc/Project02/screenshots/03.png "")
![](https://bytebucket.org/R4S3C/100daysofswift/raw/4a7e8cd0379d3e64690eaf248409df33e4243cdc/Project02/screenshots/04.png "")
![](https://bytebucket.org/R4S3C/100daysofswift/raw/4a7e8cd0379d3e64690eaf248409df33e4243cdc/Project02/screenshots/05.png "")