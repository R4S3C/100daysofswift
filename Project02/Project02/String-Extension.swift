//
//  String-Extension.swift
//  Project02
//
//  Created by Cesar Palma on 2/21/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import Foundation

extension String {
    
    func capitalizeCountryNames() -> String {
        if self == "us" || self == "uk" {
            return self.uppercased()
        } else {
            return self.capitalized
        }
    }
    
}
