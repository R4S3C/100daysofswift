//
//  ViewController.swift
//  Project02
//
//  Created by Cesar Palma on 2/17/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit

class ViewController: UIViewController, Storyboarded {

    @IBOutlet weak var button1: R4Button!
    @IBOutlet weak var button2: R4Button!
    @IBOutlet weak var button3: R4Button!
    
    var countries = [String]()
    var score = 0 {
        didSet {
            showScore()
        }
    }
    var correctAnswer = 0
    var tries = 0
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showScore()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
        countries += ["estonia", "france", "germany", "ireland", "italy", "monaco", "nigeria", "poland", "russia", "spain", "uk", "us"]
        
        askQuestion()
    }
    
    private func askQuestion(action: UIAlertAction! = nil) {
        countries.shuffle()
        correctAnswer = Int.random(in: 0...2)
        title = countries[correctAnswer].uppercased()
        
        button1.setFlag(for: countries[0])
        button2.setFlag(for: countries[1])
        button3.setFlag(for: countries[2])
    }
    
    private func showResults(action: UIAlertAction! = nil) {
        let showResultsVC = ShowResultsViewController.instantiate()
        showResultsVC.score = score
        navigationController?.present(showResultsVC, animated: true, completion: {
            self.score = 0
            self.tries = 0
        })
    }
    
    private func showScore() {
        let label = UILabel()
        label.text = "Score: \(score)"
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: label)
    }

    @IBAction func buttonTapped(_ sender: R4Button) {
        var title: String
        var message: String
        tries += 1
        
        if sender.tag == correctAnswer {
            title = "👍 Correct"
            message = "Keep going!"
            score += 1
        } else {
            title = "👎 Wrong"
            message = "That's the flag of \(countries[sender.tag].capitalizeCountryNames())"
            score -= 1
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if tries == 10 {
            alertController.addAction(UIAlertAction(title: "Show Results", style: .default, handler: showResults))
        } else {
            alertController.addAction(UIAlertAction(title: "Continue", style: .default, handler: askQuestion))
        }
        
        present(alertController, animated: true)
    }
    
}

