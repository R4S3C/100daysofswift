//
//  ShowResultsViewController.swift
//  Project02
//
//  Created by Cesar Palma on 2/21/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit

class ShowResultsViewController: UIViewController, Storyboarded {

    @IBOutlet weak var emojiLabel: UILabel!
    @IBOutlet weak var resultsLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var retryButton: R4Button!
    
    var score: Int?
    var messageForSharing: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        retryButton.setRetryAppearance()

        if let totalScore = score {
            resultsMessage(for: totalScore)
            scoreLabel.text = "Score: \(totalScore)"
            messageForSharing = "My score in Guess the Flag: \(totalScore)"
        }
    }
    
    @IBAction func tryAgainTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func shareTapped(_ sender: UIButton) {
        guard let message = messageForSharing else { return }
        
        let activityVC = UIActivityViewController(activityItems: [message], applicationActivities: [])
        present(activityVC, animated: true, completion: nil)
    }
    
    private func resultsMessage(for score: Int) {
        switch score {
        case 0...3:
            emojiLabel.text = "🤦🏻‍♂️"
            resultsLabel.text = "Not even close"
        case 3...6 :
            emojiLabel.text = "✌🏻"
            resultsLabel.text =  "You could do better"
        case 7...9:
            emojiLabel.text = "👍"
            resultsLabel.text =  "Great job"
        case 10:
            emojiLabel.text = "🏆"
            resultsLabel.text =  "Like a ninja"
        default:
            emojiLabel.text = "💩"
            resultsLabel.text =  "What?!"
        }
    }
    
}
