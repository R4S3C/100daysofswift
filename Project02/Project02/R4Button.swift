//
//  R4Button.swift
//  Project02
//
//  Created by Cesar Palma on 2/19/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit

class R4Button: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    
    private func setupButton() {
        layer.borderWidth = 2
        layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func setFlag(for countryName: String) {
        setImage(UIImage(named: countryName), for: .normal)
    }
    
    func setRetryAppearance() {
        layer.borderWidth = 0
        layer.borderColor = UIColor.clear.cgColor
        layer.cornerRadius = 12
        clipsToBounds = true
        setBackgroundImage(UIImage(named: "launcScreen-bg"), for: .normal)
        setTitleColor(.white, for: .normal)
        titleLabel?.font = .boldSystemFont(ofSize: 24)
        setTitle("Play again!", for: .normal)
    }
    
}

