//
//  Array-Extension.swift
//  ChallengeD50
//
//  Created by Cesar Palma on 3/23/19.
//  Copyright © 2019 RɅSΞC. All rights reserved.
//

import Foundation

extension Array where Element: Codable {
    func save() {
        let jsonEncoder = JSONEncoder()
        var key = String()
        if let savedData = try? jsonEncoder.encode(self) {
            let defaults = UserDefaults.standard
            
            if Element.self == Photo.self {
                key = "photos"
                defaults.set(savedData, forKey: key)
            } else {
                print("failed to save \(key)")
            }
        }
    }
    
    func retrieveFromDisk(forKey: String) -> Array {
        let defaults = UserDefaults.standard
        let jsonDecoder = JSONDecoder()
        var arrayRetrieved = Array()
        
        switch forKey {
        case "photos":
            if let photosData = defaults.object(forKey: forKey) as? Data {
                do {
                    arrayRetrieved = try jsonDecoder.decode([Photo].self, from: photosData) as! Array<Element>
                } catch {
                    print("Failed to load ArrayRetrieved")
                }
            }
        default:
            print("What?!")
        }
        
        return arrayRetrieved
    }
}


