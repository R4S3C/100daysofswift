//
//  FileManager-Extension.swift
//  ChallengeD50
//
//  Created by Cesar Palma on 3/23/19.
//  Copyright © 2019 RɅSΞC. All rights reserved.
//

import Foundation

extension FileManager {
    static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}
