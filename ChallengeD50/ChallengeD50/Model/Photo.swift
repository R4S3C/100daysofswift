//
//  Photo.swift
//  ChallengeD50
//
//  Created by Cesar Palma on 3/23/19.
//  Copyright © 2019 RɅSΞC. All rights reserved.
//

import Foundation

struct Photo: Codable {
    let title: String
    let imageName: String
}
