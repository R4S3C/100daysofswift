//
//  PhotoCell.swift
//  ChallengeD50
//
//  Created by Cesar Palma on 3/23/19.
//  Copyright © 2019 RɅSΞC. All rights reserved.
//

import UIKit

class PhotoCell: UITableViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var photo: Photo? {
        didSet {
            guard let photo = photo else { fatalError("No photo found") }
            
            let imagePath = FileManager.getDocumentsDirectory().appendingPathComponent(photo.imageName)
            photoImageView.image = UIImage(contentsOfFile: imagePath.path)
            titleLabel.text = photo.title
        }
    }

}
