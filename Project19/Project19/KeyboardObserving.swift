//
//  KeyboardObserving.swift
//  Project19
//
//  Created by Cesar Palma on 4/11/19.
//  Copyright © 2019 RɅSΞC. All rights reserved.
//

import UIKit

protocol KeyboardObserving: class {
    func keyboardWillShow(with size: CGSize)
    func keyboardWillHide()
}

extension KeyboardObserving {
    func addKeyboardObservers(to notification: NotificationCenter) {
        notification.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) { [weak self] notification in
            let key = UIResponder.keyboardFrameEndUserInfoKey
            guard let keyboardSizeValue = notification.userInfo?[key] as? NSValue else { return }
            
            let keyboardSize = keyboardSizeValue.cgRectValue
            self?.keyboardWillShow(with: keyboardSize.size)
        }
        
        notification.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil)  { [weak self] _ in
                self?.keyboardWillHide()
        }
    }
    
    public func removeKeyboardObservers(from notificationCenter: NotificationCenter) {
        notificationCenter.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        notificationCenter.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
}
