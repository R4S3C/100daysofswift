//
//  DetailViewController.swift
//  Project07
//
//  Created by Cesar Palma on 3/6/19.
//  Copyright © 2019 Cesar Palma. All rights reserved.
//

import UIKit
import WebKit

class DetailViewController: UIViewController {

    var webView: WKWebView!
    var detailItem: Petition?
    
    override func loadView() {
        webView = WKWebView()
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.largeTitleDisplayMode = .never

        guard let detailItem = detailItem else { return }
        
        let html = """
        <html>
            <head>
                <meta name="viewport" content="width=device-width initial-scale=1">
                <style>
                    h3   { font-size: 120%; font-family: -apple-system;  }
                    body { font-size: 100%; font-family:  -apple-system; }
                    p { font-family: -apple-system; }
                </style>
            </head>
            <body>
                <h3 style="padding: 10 10 0 10">\(detailItem.title)</h3>
                <p style="padding: 0 10 0 10">\(detailItem.body)</p>
            </body>
        </html>
        """
        
        webView.loadHTMLString(html, baseURL: nil)
    }

}


